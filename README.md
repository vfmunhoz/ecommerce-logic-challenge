# Challenge

Para executar siga os passos abaixo, a string de entrada para o stream deve ser informada pelo string args do metodo main. Apenas o primeiro parametro do args esta sendo considerado!

```bash
mvn clean package
java -jar target/ecommerce-logic-challenge-0.0.1-SNAPSHOT-jar-with-dependencies.jar aAbBABacafe
```