package br.com.ecommerce.main;

import br.com.ecommerce.stream.Stream;
import br.com.ecommerce.stream.StreamImpl;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class CharFinderTest {

	private CharFinder charFinder;
	
	@Before
	public void setup() {
		this.charFinder = new CharFinder();
	}
	
	@Test
	public void testSampleCase() {
		final String input = "aAbBABacafe";
		Stream stream = new StreamImpl(input);
		char c = this.charFinder.firstChar(stream);
		
		assertEquals('e', c);
	}
	
	@Test
	public void testNoCharCase() {
		final String input = "eAbBABacafe";
		Stream stream = new StreamImpl(input);
		char c = this.charFinder.firstChar(stream);
		
		assertEquals('0', c);
	}
	
	@Test
	public void testMiddleCase() {
		final String input = "eAbiBABacafe";
		Stream stream = new StreamImpl(input);
		char c = this.charFinder.firstChar(stream);
		
		assertEquals('i', c);
	}
	
	@Test
	public void testTwoCases() {
		final String input = "aAbiBABacafe";
		Stream stream = new StreamImpl(input);
		char c = this.charFinder.firstChar(stream);
		
		assertEquals('i', c);
	}
	
	@Test
	public void testSecondCaseValid() {
		final String input = "aAbiBABacafei";
		Stream stream = new StreamImpl(input);
		char c = this.charFinder.firstChar(stream);
		
		assertEquals('e', c);
	}
}
