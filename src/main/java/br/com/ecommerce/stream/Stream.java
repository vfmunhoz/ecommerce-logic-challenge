package br.com.ecommerce.stream;

public interface Stream {
	
	public char getNext();
    public boolean hasNext();
}
