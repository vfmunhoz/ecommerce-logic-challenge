package br.com.ecommerce.stream;

public class StreamImpl implements Stream {

	private final String stream;
	private int streamPosition;
	
	public StreamImpl(final String stream) {
		this.stream = stream;
		this.streamPosition = 0;
	}
	
	public char getNext() {
		return this.stream.charAt(this.streamPosition++);
	}

	public boolean hasNext() {
		return this.streamPosition < (this.stream.length());
	}

}