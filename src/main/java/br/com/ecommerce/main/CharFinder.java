package br.com.ecommerce.main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.ecommerce.stream.Stream;
import br.com.ecommerce.stream.StreamImpl;

public class CharFinder {

	public static void main(String[] args) {
		if(args.length < 1) {
			System.out.println("Por favor envie como parametro a string que deve ser avaliada. (args)");
			return;
		}
		
		final Stream stream = new StreamImpl(args[0]); // Creates the stream
		final CharFinder charFinder = new CharFinder(); // Creates the class instance
		final char charToBeDisplayed = charFinder.firstChar(stream); // Execute the search
		
		if(charToBeDisplayed != '0') {
			System.out.println("O caracter encontrado foi: " + charToBeDisplayed);
		} else {
			System.out.println("Nao foi possivel encontrar um caracter.");
		}
	}

	public CharFinder() {
	}
	
	/**
	 * Makes the search in the stream
	 * @param stream
	 * @return
	 */
	public char firstChar(Stream stream) {
		final Map<Character, Integer> repetitions = new HashMap<Character, Integer>(); // Map to count the repetitions
		final char[] lastChars = new char[3]; // Last 3 positions of the stream

		List<Character> possibleResults = new ArrayList<Character>();
		
		while(stream.hasNext()) {
			char c = stream.getNext();
			
			this.includeCharOnArray(c, lastChars);
			char tempChar = this.checkResult(lastChars, repetitions);
			
			if(tempChar != '0') { // If you have founded the possibility
				possibleResults.add(tempChar);
			}
		}
		
		return this.computeResult(possibleResults, repetitions);
	}
	
	/** 
	 * Computes and return the result
	 * 
	 * @param possibleResults
	 * @param repetitions
	 * @return
	 */
	private char computeResult(List<Character> possibleResults, Map<Character, Integer> repetitions) {
		for(char c : possibleResults) {
			if(repetitions.get(Character.toLowerCase(c)) == 0) {
				return c;
			}
		}

		return '0';
	}

	/**
	 * Check if the rule is ok!
	 * @param lastChars
	 * @param repetitions
	 * @return 0 if the sequence doesnt match the rules else the vogal
	 */
	private char checkResult(char[] lastChars, Map<Character, Integer> repetitions) {
		boolean firstPosition = !Character.isDigit(lastChars[0]) && this.isVogal(lastChars[0]); // first must be vogal
		boolean secondPosition = !Character.isDigit(lastChars[1]) && !this.isVogal(lastChars[1]); // second must not be vogal
		boolean thirdPosition = !Character.isDigit(lastChars[2]) && this.isVogal(lastChars[2]); // third must be vogal
		boolean hasRepetition = !this.checkRepetition(lastChars[2], repetitions); // third must not have repetitions
		
		return (firstPosition && secondPosition && thirdPosition && hasRepetition) ? lastChars[2] : '0';
	}

	/**
	 * Check if the vogal has repetitions in the stream
	 * @param c
	 * @param repetitions
	 * @return
	 */
	private boolean checkRepetition(char c, Map<Character, Integer> repetitions) {
		char lowerCaseChar = Character.toLowerCase(c);
		
		if(repetitions.containsKey(lowerCaseChar)) {
			repetitions.put(lowerCaseChar, repetitions.get(lowerCaseChar) + 1);
			return true;
		} else {
			repetitions.put(lowerCaseChar, 0);
			return false;
		}
	}

	/**
	 * Walks the array
	 * @param c
	 * @param lastChars
	 */
	private void includeCharOnArray(char c, char[] lastChars) {
		lastChars[0] = lastChars[1];
		lastChars[1] = lastChars[2];
		lastChars[2] = c;
	}

	/**
	 * Return true if the char c is a vogal
	 * @param c
	 * @return
	 */
	private boolean isVogal(char c) {
		switch (c) {
			case 'a':
			case 'A':
			case 'e':
			case 'E':
			case 'i':
			case 'I':
			case 'o':
			case 'O':
			case 'u':
			case 'U':
				return true;
			default:
				return false;
		}
	}
}
